# Site web de l'équipe Les Mangoustes
## Description
Le principe de cette application est de permettre aux organisations de publier des bons plans ou conseils pour aider les étudiants dans la vie de tout les jours.

Elle est découpée en microservices (Back, gestion des publications et gestions des utilisateurs) en plus du front-end.

C'est un prototype non fonctionnel pour illustrer l'architecture microservices couplée a docker.

## Instalation
Le projet possède 2 dépendences : docker et docker-compose
Pour lancer l'application :
+ cloner le dépot à cette adresse https://gitlab.com/-/ide/project/nuit-info-2019/application/
+ lancer l'application avec docker-compose up

Le site est alors disponible sur localhost.
